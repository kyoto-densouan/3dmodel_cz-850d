# README #

1/3スケールのSHARP X-1turbo用ディスプレイ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。
背面は情報が少ないため実機と異なると思います。
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

組み込む液晶パネルは以下を想定しています。
VGA AV Lcd Controller board KYV-N2 V1
5inch AT050TN22 640x480 lcd panel

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-850d/raw/e057daef2243f0ffda598cc7f320b918fdef9bc7/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-850d/raw/e057daef2243f0ffda598cc7f320b918fdef9bc7/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-850d/raw/e057daef2243f0ffda598cc7f320b918fdef9bc7/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cz-850d/raw/e057daef2243f0ffda598cc7f320b918fdef9bc7/ExampleImage_2.jpg)
